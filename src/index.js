import{createClient} from "libsql-stateless-easy";

export default{
    async fetch(request, env, ctx){
        const corsHeaders = {
            "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Methods": "GET, HEAD, POST, OPTIONS",
            "Access-Control-Allow-Headers": "*",
            "Content-Type": "application/json"
        };

        switch(request.method){
            case "OPTIONS": return handleOptions(request);
            case "POST": return handlePost(request, env);
            default: return new Response(null, {status: 405,statusText: "Nah Mate"});
        };

        function handleOptions(request){
            if(request.headers.get("Origin") !== null && request.headers.get("Access-Control-Request-Method") !== null && request.headers.get("Access-Control-Request-Headers") !== null){
                return new Response(null, {headers: corsHeaders});
            }else{
                return new Response(null, {headers: {"Allow": "GET, HEAD, POST, OPTIONS"}});
            }
        };

        async function handlePost(request, env){
            function where(search, group, type){
                if(search === "" && group === "\u2736" && type === "\u2736"){
                    return "";
                };
                let where = "WHERE ";
                if(type != "\u2736"){
                    where += `"type" = "${type}"`;
                }else if(group != "\u2736"){
                    where += `"group" = "${group}"`;
                };
                if(search != ""){
                    if(group != "\u2736" && type != "\u2736"){
                        where += " AND ";
                    };
                    where += `lower("title") LIKE "%${search.toLowerCase()}%"`;
                };
                return where;
            };

            function orderBy(sort){
                switch(sort){
                    case "A-Z": return 'ORDER BY "title" ASC';
                    case "Z-A": return 'ORDER BY "title" DESC';
                    case "NEW": return 'ORDER BY "date" ASC';
                    case "OLD": return 'ORDER BY "date" DESC';
                    default: return 'ORDER BY "hash" ASC';
                };
            };

            function offset(page){
                return `LIMIT 20 OFFSET ${page * 20}`;
            };

            const client = createClient({
                url: `${env.TURSO_URL}`,
                authToken: `${env.TURSO_AUTH_KEY}`
            });

            const req = await request.json();

            async function queryDB(){
                const rs = await client.execute(`SELECT "title", "group", "type", "name", "key", "bpm", "waveform" FROM samples ${where(req.search, req.group, req.type)} ${orderBy(req.sort)} ${offset(req.page)};`);
                return rs.rows;
            };

            let res = await queryDB();
            return new Response(JSON.stringify(res), {headers: corsHeaders});
        };
	}
};
